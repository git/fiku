/*  gdt.hpp - GDT related functions for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdint.h>
#include <stddef.h>

namespace grace
{
    namespace gdt
    {

        /**
         * @brief Structure for a GDT entry
         *
         * @details @rst
         *
         * This structure holds a GDT entry in a format that is understood by
         * processor natively. The processor can read the GDT entry store in
         * this structure directly without any encoding. Pointer to a GDT entry
         * can also be casted to a pointer of this structure to inspect and
         * modify the entry directly.
         *
         * @endrst
         */
        struct entry
        {
            private:

                // The lower 16 bit of limit
                uint16_t limit_low__:16;

                // The lower 24 bit of base
                uint32_t base_low__:24;

                // Access bit, automatically set to true by CPU when the segment
                // is accessed (set to false initially)
                volatile bool accessed__:1;

                // Read bit for code segments, write bit for data segments
                bool rw_bit__:1;

                // Direction bit for data segments, conforming bit for code
                // segments
                bool dc_bit__:1;

                // Whether the segment is executable
                bool executable__:1;

                // Descriptor type, always 1 or true
                bool descriptor_type__:1;

                // Privilege level of descriptor
                uint8_t privilege_level__:2;

                // Whether the segment is present
                bool segment_present__:1;

                // Higher 4 bit of limit
                uint8_t limit_high__:4;

                // Available for system use
                volatile bool system_use__:1;

                // Whether the segment is a long mode (x86_64) code segment
                bool long_mode_cs__:1;
                // TODO: Use it when x86_64 is supported

                // Size bit, false or 0 means 16 bit protected mode, true or 0
                // means 32 bit protected mode
                bool size_bit__:1;

                // Granularity
                bool granularity__:1;

                // Higher 8 bit of base
                uint8_t base_high__:8;

            public:

                /**
                 * @brief Constructs new a GDT entry
                 *
                 * @details @rst
                 *
                 * This constructor constructs a new null GDT entry.
                 *
                 * @endrst
                 */
                constexpr entry()
                    : entry(0, 0, 0, 0, 0, 0, 0, 0, 0)
                {}

                /**
                 * @brief Constructs a new GDT entry object
                 *
                 * @details @rst
                 *
                 * This constructor constructs a new GDT entry with given
                 * parameters. For detailed description for parameters, see
                 * Intel manual.
                 *
                 * @endrst
                 *
                 * @param base the base address of the segment
                 * @param limit the limit of the segment, or the segments length
                 * @param executable whether data the segment is executable,
                 * ``true`` for code segments, ``false`` for data segments
                 * @param rw_bit determines whether the segment readable if
                 * ``executable`` param set to ``true``, otherwise determines
                 * whether the segment is writable. (Reading is always allowed
                 * for a data segment and writing is never allowed for a code
                 * segment)
                 * @param dc_bit determines whether the segment is conforming if
                 * parameter ``executable`` is set to ``true``, otherwise
                 * whether the segment grows up (when set to ``false``) or down
                 * (when set to ``true``).
                 * @param privilege_level privilege level of segment, the ring
                 * from where the segment can be accessed
                 * @param size_bit the size of bit of the segment, 0 or
                 * ``false`` means 16 bit protected mode, 1 or ``true`` means 32
                 * bit protected mode
                 * @param long_mode determines whether the segment is a long
                 * mode code, non effective when segment is not executable
                 * @param present whether the segment is present, should be
                 * ``true`` unless there any specific reason for setting to
                 * ``false``
                 */
                constexpr entry(
                    uint32_t base,
                    uint32_t limit,
                    bool executable,
                    bool rw_bit,
                    bool dc_bit,
                    uint8_t privilege_level = 0,
                    bool size_bit = 1,
                    bool long_mode = 0,
                    bool present = true
                )
                    : limit_low__(0),
                      base_low__(0),
                      accessed__(0),
                      rw_bit__(rw_bit),
                      dc_bit__(dc_bit),
                      executable__(executable),
                      descriptor_type__(1),
                      privilege_level__(privilege_level),
                      segment_present__(present),
                      limit_high__(0),
                      system_use__(0),
                      long_mode_cs__(long_mode),
                      size_bit__(size_bit),
                      granularity__(0),
                      base_high__(0)
                {
                    this->base(base);
                    this->limit(limit);
                }

                /**
                 * @brief Returns the base of the segment
                 *
                 * @return the base of the segment
                 */
                constexpr uint32_t base() const
                {

                    // Combine higher 8 and lower 24 bits, and return
                    return base_high__ << 24 | base_low__;
                }

                /**
                 * @brief Sets the base of the segment
                 *
                 */
                constexpr void base(uint32_t value)
                {
                    base_low__ = value & 0xffffff;
                    base_high__ = value >> 24 & 0xff;
                }

                /**
                 * @brief Returns the limit of the segment
                 *
                 * @return the limit of the segment
                 */
                constexpr uint32_t limit() const
                {

                    // Combine higher 4 and lower 16 bits
                    uint32_t limit = limit_high__ << 16 | limit_low__;

                    // Convert limit info bytes if required
                    if (granularity__ == 1)
                    {
                        limit = limit << 12 | 0xfff;
                    }

                    return limit;
                }

                /**
                 * @brief Sets the limit of the segment
                 *
                 */
                constexpr void limit(uint32_t value)
                {

                    // Change granularity if required
                    if (value > 0xfffff)
                    {
                        granularity__ = 1;
                        value >>= 12;
                    }

                    limit_low__ = value & 0xffff;
                    limit_high__ = value >> 16 & 0xf;
                }

                /**
                 * @brief Returns whether the segment is executable
                 *
                 * @return whether the segment is executable
                 */
                constexpr bool executable() const
                {
                    return executable__;
                }

                /**
                 * @brief Sets whether the segment is executable
                 *
                 */
                constexpr void executable(bool value)
                {
                    executable__ = value;
                }

                /**
                 * @brief Returns the privilege level of the segment
                 *
                 * @return the privilege level of the segment
                 */
                constexpr uint8_t privilege_level() const
                {
                    return privilege_level__;
                }

                /**
                 * @brief Sets the privilege level of the segment
                 *
                 */
                constexpr void privilege_level(uint8_t level)
                {
                    privilege_level__ = level & 0b11;
                }

                /**
                 * @brief Returns the whether the segment is present
                 */
                constexpr bool present() const
                {
                    return segment_present__;
                }

                /**
                 * @brief Sets the availablity of the segment
                 */
                constexpr void present(bool value)
                {
                    segment_present__ = value;
                }

                /**
                 * @brief Returns the RW bit of the segment
                 *
                 * @return the RW bit of the segment
                 */
                constexpr bool rw_bit() const
                {
                    return rw_bit__;
                }

                /**
                 * @brief Sets the RW bit of the segment
                 *
                 */
                constexpr void rw_bit(bool value)
                {
                    rw_bit__ = value;
                }

                /**
                 * @brief Returns the DC bit of the segment
                 *
                 * @return the DC bit of the segment
                 */
                constexpr bool dc_bit() const
                {
                    return dc_bit__;
                }

                /**
                 * @brief Sets the DC bit of the segment
                 *
                 */
                constexpr void dc_bit(bool value)
                {
                    dc_bit__ = value;
                }

                /**
                 * @brief Returns the size bit of the segment
                 *
                 * @return the size bit of the segment
                 */
                constexpr bool size_bit() const
                {
                    return size_bit__;
                }

                /**
                 * @brief Sets the size bit of the segment
                 *
                 */
                constexpr void size_bit(bool value)
                {
                    size_bit__ = value;
                }

                /**
                 * @brief Returns whether the segment is readable
                 *
                 * @return whether the segment is readable
                 */
                constexpr bool readable() const
                {

                    // For a segment to be readable, either it must be a data
                    // segment (i.e not executable) or the RW bit must be set to
                    // 1
                    return !executable__ || rw_bit__;
                }

                /**
                 * @brief Returns whether the segment is writable
                 *
                 * @return whether the segment is writable
                 */
                constexpr bool writable() const
                {

                    // For a segment to be readable, it must be a data segment
                    // (i.e not executable) and the RW bit must be set to 1
                    return !executable__ && rw_bit__;
                }

                /**
                 * @brief Returns whether the segment is conforming
                 *
                 * @return whether the segment is conforming
                 */
                constexpr bool conforming() const
                {

                    // A segment is conforming when it is a code segment and
                    // it's DC bit is set to 1
                    return executable__ && dc_bit__;
                }

                /**
                 * @brief Returns the direction of growing (up or down) of the
                 * segment
                 *
                 * @details @rst
                 *
                 * This returns the direction of growing (up or down) of the
                 * segment. If it returns ``false`` or 0, the segment grows
                 * downwards, otherwise the segment grows upwards. See
                 * https://wiki.osdev.org/Expand_Down#Expand_Down for details.
                 *
                 * @endrst
                 *
                 * @return returns ``false`` or 0 when the segment grows
                 * downwards, return ``true`` or 1 when the segment grows
                 * upwards
                 */
                constexpr bool direction() const
                {

                    // The DC bit of an entry holds the direction only when the
                    // segment is a data segment
                    return !executable__ && dc_bit__;
                }

                /**
                 * @brief Returns whether the segment is accessed by the CPU
                 *
                 * @return whether the segment is accessed by the CPU
                 */
                inline bool accessed() const
                {

                    // CPU automatically sets this to true when the segment is
                    // accessed
                    return accessed__;
                }
        }
#ifndef DOXYGEN_DOCUMENTATION_BUILD
            __attribute__((packed))
#endif
        ;

        /**
         * @brief Flushes a GDT to CPU
         *
         * @details @rst
         *
         * This function flushes a GDT to CPU.
         *
         * .. note::
         *
         *      The entry of after the null entry (entry at index 1) must be the
         *      kernel code segment and the next one must be the kernel data
         *      segment, otherwise things may (and probably will) go wrong.
         *
         * @endrst
         *
         * @param table pointer to first null entry of GDT
         * @param count count of GDT entries
         */
        void flush(const entry* table, size_t count);

        /**
         * @brief Sets up a GDT
         *
         * @details @rst
         *
         * This function sets up and loads a GDT with 16 entries. The GDT
         * contains a null entry, followed by a entry of kernel code segment and
         * a entry for kernel data segment. All other entries are set to null
         * entries. If any error occurs, ``nullptr`` is returned.
         *
         * .. warning::
         *
         *      Calling this function will overwrite any GDT returned previously
         *      by this function. Be sure to backup the previous GDT before
         *      calling this function again!
         *
         * @endrst
         *
         * @param flush whether to flush the GDT
         * @return pointer to the GDT
         */
        entry* init(bool flush = true);
    }
}
