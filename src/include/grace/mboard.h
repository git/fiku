/*  mboard.h - Hardware related low-level C functions

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Send a byte to a port
 *
 * @param port the port
 * @param data the byte to send
 */
void mboard_outb(uint16_t port, uint8_t data);

/**
 * @brief Gets a byte from a port
 * 
 * @param port the port
 * @return a byte from the port
 */
uint8_t mboard_inb(uint16_t port);

/**
 * @brief Gets a word from a port
 *
 * @details @rst
 *
 * This function gets a word from given port. A word is two bytes of the data.
 * See :ref:`the definition of word <definition-word>`.
 *
 * @endrst
 *
 * @param port the port
 * @return a word from the port
 */
uint16_t mboard_inw(uint16_t port);

#ifdef __cplusplus
}
#endif
