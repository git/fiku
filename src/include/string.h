/*  string.h - Null-terminated byte string related C functions

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stddef.h>

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * @brief Checks whether a character is alphanumeric
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is alphanumeric
 */
bool isalnum(int ch);

/**
 * @brief Checks whether a character is alphabetic
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is alphabetic
 */
bool isalpha(int ch);

/**
 * @brief Checks whether a character is lowercase
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is lowercase
 */
bool islower(int ch);

/**
 * @brief Checks whether a character is uppercase
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is uppercase
 */
bool isupper(int ch);

/**
 * @brief Checks whether a character is a digit
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a digit
 */
bool isdigit(int ch);

/**
 * @brief Checks whether a character is a hexadecimal digit
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a hexadecimal digit
 */
bool isxdigit(int ch);

/**
 * @brief Checks whether a character is a control character
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a control character
 */
bool iscntrl(int ch);

/**
 * @brief Checks whether a character is a graphical character
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a graphical character
 */
bool isgraph(int ch);

/**
 * @brief Checks whether a character is a space character
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a space character
 */
bool isspace(int ch);

/**
 * @brief Checks whether a character is a blank character
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a blank character
 */
bool isblank(int ch);

/**
 * @brief Checks whether a character is a print character
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a print character
 */
bool isprint(int ch);

/**
 * @brief Checks whether a character is a punctuation character
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return whether ``ch`` is a punctuation character
 */
bool ispunct(int ch);

/**
 * @brief Returns the lowercase of a character
 *
 * @details @rst
 *
 * This functions returns the returns the lowercase version a character ``ch``.
 *
 * .. note::
 *
 *      If ``ch`` is not a uppercase character, ``ch`` is returned unmodified.
 *
 * @endrst
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return lowercase version of ``ch``
 */
int tolower(int ch);

/**
 * @brief Returns the uppercase of a character
 *
 * @details @rst
 *
 * This functions returns the returns the uppercase version a character ``ch``.
 *
 * .. note::
 *
 *      If ``ch`` is not a lowercase character, ``ch`` is returned unmodified.
 *
 * @endrst
 *
 * @param ch a character representable by ``unsigned char`` or ``EOF``
 * @return uppercase version of ``ch``
 */
int toupper(int ch);

/**
 * @brief Fills buffer with given character
 *
 * @details @rst
 *
 * This function fill ``count`` bytes of given buffer ``dest`` with given
 * character ``ch``.
 *
 * .. note::
 *
 *      Before writing to buffer, ``ch`` is converted to ``unsigned char``.
 *
 * @endrst
 *
 * @param dest destination buffer
 * @param ch fill byte
 * @param count count of bytes to write
 * @return ``dest``
 */
void* memset(void* dest, int ch, size_t count);

/**
 * @brief Copies data from one buffer to another
 *
 * @details @rst
 *
 * This function copies ``count`` bytes of data from source buffer ``src`` to
 * destination buffer ``dest``.
 *
 * @endrst
 *
 * @param dest destination buffer
 * @param src source buffer
 * @param count count of bytes to copy
 * @return ``dest``
 */
void* memcpy(void* dest, const void* src, size_t count);

/**
 * @brief Moves data from one buffer to another
 *
 * @details @rst
 *
 * This function moves ``count`` bytes of data from source buffer ``src`` to
 * destination buffer ``dest``. The given buffers can overlap.
 *
 * @endrst
 *
 * @param dest destination buffer
 * @param src source buffer
 * @param count count of bytes to copy
 * @return ``dest``
 */
void* memmove(void* dest, const void* src, size_t count);

#ifdef __cplusplus
}
#endif
