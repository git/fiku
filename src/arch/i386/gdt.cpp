/*  gdt.cpp - GDT related functions for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <grace/gdt.hpp>

#ifndef __i386__
#error Loading GDT is currently only supported for i386 architecture :(
#endif

extern "C" void gdt_flush__(void* gdt_ptr);

namespace
{
    struct pointer
    {

        // Size of GDT
        uint16_t limit;

        // Base address of GDT
        uint32_t base;
    }
        __attribute__((packed));

    pointer gdt_ptr;
}

namespace grace::gdt
{
    void flush(const entry* table, size_t count)
    {
        gdt_ptr =
            {
            static_cast<uint16_t>(sizeof(entry) * count - 1),
            reinterpret_cast<uint32_t>(table)
        };
        gdt_flush__(&gdt_ptr);
    }

    entry* init(bool flush)
    {
        static entry gdt[16];

        gdt[1] = entry(0, 0xffffffff, true, true, false, 0x0);
        gdt[2] = entry(0, 0xffffffff, false, true, false, 0x0);

        for (uint8_t i = 3; i < 16; i++)
        {
            gdt[i] = entry();
        }

        if (flush)
        {
            grace::gdt::flush(gdt, 16);
        }

        return gdt;
    }
}
