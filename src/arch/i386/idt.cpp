/*  idt.cpp - IDT related functions for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <grace/idt.hpp>
#include <grace/mboard.h>

namespace
{
    struct pointer
    {

        // Size of IDT
        uint16_t limit;

        // Base address of IDT
        uint32_t base;
    }
        __attribute__((packed));

    pointer idt_ptr;
}

extern "C" void idt_flush__(void* idt_ptr);

#define DEF_ISR(n) extern "C" void isr_##n##__()

DEF_ISR(0x00);
DEF_ISR(0x01);
DEF_ISR(0x02);
DEF_ISR(0x03);
DEF_ISR(0x04);
DEF_ISR(0x05);
DEF_ISR(0x06);
DEF_ISR(0x07);
DEF_ISR(0x08);
DEF_ISR(0x09);
DEF_ISR(0x0a);
DEF_ISR(0x0b);
DEF_ISR(0x0c);
DEF_ISR(0x0d);
DEF_ISR(0x0e);
DEF_ISR(0x0f);
DEF_ISR(0x10);
DEF_ISR(0x11);
DEF_ISR(0x12);
DEF_ISR(0x13);
DEF_ISR(0x14);
DEF_ISR(0x15);
DEF_ISR(0x16);
DEF_ISR(0x17);
DEF_ISR(0x18);
DEF_ISR(0x19);
DEF_ISR(0x1a);
DEF_ISR(0x1b);
DEF_ISR(0x1c);
DEF_ISR(0x1d);
DEF_ISR(0x1e);
DEF_ISR(0x1f);
DEF_ISR(0x20);
DEF_ISR(0x21);
DEF_ISR(0x22);
DEF_ISR(0x23);
DEF_ISR(0x24);
DEF_ISR(0x25);
DEF_ISR(0x26);
DEF_ISR(0x27);
DEF_ISR(0x28);
DEF_ISR(0x29);
DEF_ISR(0x2a);
DEF_ISR(0x2b);
DEF_ISR(0x2c);
DEF_ISR(0x2d);
DEF_ISR(0x2e);
DEF_ISR(0x2f);
DEF_ISR(0x30);
DEF_ISR(0x31);
DEF_ISR(0x32);
DEF_ISR(0x33);
DEF_ISR(0x34);
DEF_ISR(0x35);
DEF_ISR(0x36);
DEF_ISR(0x37);
DEF_ISR(0x38);
DEF_ISR(0x39);
DEF_ISR(0x3a);
DEF_ISR(0x3b);
DEF_ISR(0x3c);
DEF_ISR(0x3d);
DEF_ISR(0x3e);
DEF_ISR(0x3f);
DEF_ISR(0x40);
DEF_ISR(0x41);
DEF_ISR(0x42);
DEF_ISR(0x43);
DEF_ISR(0x44);
DEF_ISR(0x45);
DEF_ISR(0x46);
DEF_ISR(0x47);
DEF_ISR(0x48);
DEF_ISR(0x49);
DEF_ISR(0x4a);
DEF_ISR(0x4b);
DEF_ISR(0x4c);
DEF_ISR(0x4d);
DEF_ISR(0x4e);
DEF_ISR(0x4f);
DEF_ISR(0x50);
DEF_ISR(0x51);
DEF_ISR(0x52);
DEF_ISR(0x53);
DEF_ISR(0x54);
DEF_ISR(0x55);
DEF_ISR(0x56);
DEF_ISR(0x57);
DEF_ISR(0x58);
DEF_ISR(0x59);
DEF_ISR(0x5a);
DEF_ISR(0x5b);
DEF_ISR(0x5c);
DEF_ISR(0x5d);
DEF_ISR(0x5e);
DEF_ISR(0x5f);
DEF_ISR(0x60);
DEF_ISR(0x61);
DEF_ISR(0x62);
DEF_ISR(0x63);
DEF_ISR(0x64);
DEF_ISR(0x65);
DEF_ISR(0x66);
DEF_ISR(0x67);
DEF_ISR(0x68);
DEF_ISR(0x69);
DEF_ISR(0x6a);
DEF_ISR(0x6b);
DEF_ISR(0x6c);
DEF_ISR(0x6d);
DEF_ISR(0x6e);
DEF_ISR(0x6f);
DEF_ISR(0x70);
DEF_ISR(0x71);
DEF_ISR(0x72);
DEF_ISR(0x73);
DEF_ISR(0x74);
DEF_ISR(0x75);
DEF_ISR(0x76);
DEF_ISR(0x77);
DEF_ISR(0x78);
DEF_ISR(0x79);
DEF_ISR(0x7a);
DEF_ISR(0x7b);
DEF_ISR(0x7c);
DEF_ISR(0x7d);
DEF_ISR(0x7e);
DEF_ISR(0x7f);
DEF_ISR(0x80);
DEF_ISR(0x81);
DEF_ISR(0x82);
DEF_ISR(0x83);
DEF_ISR(0x84);
DEF_ISR(0x85);
DEF_ISR(0x86);
DEF_ISR(0x87);
DEF_ISR(0x88);
DEF_ISR(0x89);
DEF_ISR(0x8a);
DEF_ISR(0x8b);
DEF_ISR(0x8c);
DEF_ISR(0x8d);
DEF_ISR(0x8e);
DEF_ISR(0x8f);
DEF_ISR(0x90);
DEF_ISR(0x91);
DEF_ISR(0x92);
DEF_ISR(0x93);
DEF_ISR(0x94);
DEF_ISR(0x95);
DEF_ISR(0x96);
DEF_ISR(0x97);
DEF_ISR(0x98);
DEF_ISR(0x99);
DEF_ISR(0x9a);
DEF_ISR(0x9b);
DEF_ISR(0x9c);
DEF_ISR(0x9d);
DEF_ISR(0x9e);
DEF_ISR(0x9f);
DEF_ISR(0xa0);
DEF_ISR(0xa1);
DEF_ISR(0xa2);
DEF_ISR(0xa3);
DEF_ISR(0xa4);
DEF_ISR(0xa5);
DEF_ISR(0xa6);
DEF_ISR(0xa7);
DEF_ISR(0xa8);
DEF_ISR(0xa9);
DEF_ISR(0xaa);
DEF_ISR(0xab);
DEF_ISR(0xac);
DEF_ISR(0xad);
DEF_ISR(0xae);
DEF_ISR(0xaf);
DEF_ISR(0xb0);
DEF_ISR(0xb1);
DEF_ISR(0xb2);
DEF_ISR(0xb3);
DEF_ISR(0xb4);
DEF_ISR(0xb5);
DEF_ISR(0xb6);
DEF_ISR(0xb7);
DEF_ISR(0xb8);
DEF_ISR(0xb9);
DEF_ISR(0xba);
DEF_ISR(0xbb);
DEF_ISR(0xbc);
DEF_ISR(0xbd);
DEF_ISR(0xbe);
DEF_ISR(0xbf);
DEF_ISR(0xc0);
DEF_ISR(0xc1);
DEF_ISR(0xc2);
DEF_ISR(0xc3);
DEF_ISR(0xc4);
DEF_ISR(0xc5);
DEF_ISR(0xc6);
DEF_ISR(0xc7);
DEF_ISR(0xc8);
DEF_ISR(0xc9);
DEF_ISR(0xca);
DEF_ISR(0xcb);
DEF_ISR(0xcc);
DEF_ISR(0xcd);
DEF_ISR(0xce);
DEF_ISR(0xcf);
DEF_ISR(0xd0);
DEF_ISR(0xd1);
DEF_ISR(0xd2);
DEF_ISR(0xd3);
DEF_ISR(0xd4);
DEF_ISR(0xd5);
DEF_ISR(0xd6);
DEF_ISR(0xd7);
DEF_ISR(0xd8);
DEF_ISR(0xd9);
DEF_ISR(0xda);
DEF_ISR(0xdb);
DEF_ISR(0xdc);
DEF_ISR(0xdd);
DEF_ISR(0xde);
DEF_ISR(0xdf);
DEF_ISR(0xe0);
DEF_ISR(0xe1);
DEF_ISR(0xe2);
DEF_ISR(0xe3);
DEF_ISR(0xe4);
DEF_ISR(0xe5);
DEF_ISR(0xe6);
DEF_ISR(0xe7);
DEF_ISR(0xe8);
DEF_ISR(0xe9);
DEF_ISR(0xea);
DEF_ISR(0xeb);
DEF_ISR(0xec);
DEF_ISR(0xed);
DEF_ISR(0xee);
DEF_ISR(0xef);
DEF_ISR(0xf0);
DEF_ISR(0xf1);
DEF_ISR(0xf2);
DEF_ISR(0xf3);
DEF_ISR(0xf4);
DEF_ISR(0xf5);
DEF_ISR(0xf6);
DEF_ISR(0xf7);
DEF_ISR(0xf8);
DEF_ISR(0xf9);
DEF_ISR(0xfa);
DEF_ISR(0xfb);
DEF_ISR(0xfc);
DEF_ISR(0xfd);
DEF_ISR(0xfe);
DEF_ISR(0xff);

#undef DEF_ISR

namespace grace::idt
{
    void flush(const gate* table, size_t count)
    {
        idt_ptr =
            {
            static_cast<uint16_t>(sizeof(gate) * count - 1),
            reinterpret_cast<uint32_t>(table)
        };

        idt_flush__(&idt_ptr);
    }

    gate* init(bool flush, bool pic)
    {

        // Remap the PIC
        if (pic)
        {
            mboard_outb(0x20, 0x11);
            mboard_outb(0xa0, 0x11);
            mboard_outb(0x21, 0x20);
            mboard_outb(0xa1, 0x28);
            mboard_outb(0x21, 0x1);
            mboard_outb(0xa1, 0x1);
            mboard_outb(0x21, 0x0);
            mboard_outb(0xa1, 0x0);
        }

        static gate idt[256];

#define SET_GATE(n) idt[n] =                                            \
            gate(                                                       \
                reinterpret_cast<uint32_t>(isr_##n##__),                \
                0x8,                                                    \
                0b1110,                                                 \
                0                                                       \
            )

        SET_GATE(0x00);
        SET_GATE(0x01);
        SET_GATE(0x02);
        SET_GATE(0x03);
        SET_GATE(0x04);
        SET_GATE(0x05);
        SET_GATE(0x06);
        SET_GATE(0x07);
        SET_GATE(0x08);
        SET_GATE(0x09);
        SET_GATE(0x0a);
        SET_GATE(0x0b);
        SET_GATE(0x0c);
        SET_GATE(0x0d);
        SET_GATE(0x0e);
        SET_GATE(0x0f);
        SET_GATE(0x10);
        SET_GATE(0x11);
        SET_GATE(0x12);
        SET_GATE(0x13);
        SET_GATE(0x14);
        SET_GATE(0x15);
        SET_GATE(0x16);
        SET_GATE(0x17);
        SET_GATE(0x18);
        SET_GATE(0x19);
        SET_GATE(0x1a);
        SET_GATE(0x1b);
        SET_GATE(0x1c);
        SET_GATE(0x1d);
        SET_GATE(0x1e);
        SET_GATE(0x1f);
        SET_GATE(0x20);
        SET_GATE(0x21);
        SET_GATE(0x22);
        SET_GATE(0x23);
        SET_GATE(0x24);
        SET_GATE(0x25);
        SET_GATE(0x26);
        SET_GATE(0x27);
        SET_GATE(0x28);
        SET_GATE(0x29);
        SET_GATE(0x2a);
        SET_GATE(0x2b);
        SET_GATE(0x2c);
        SET_GATE(0x2d);
        SET_GATE(0x2e);
        SET_GATE(0x2f);
        SET_GATE(0x30);
        SET_GATE(0x31);
        SET_GATE(0x32);
        SET_GATE(0x33);
        SET_GATE(0x34);
        SET_GATE(0x35);
        SET_GATE(0x36);
        SET_GATE(0x37);
        SET_GATE(0x38);
        SET_GATE(0x39);
        SET_GATE(0x3a);
        SET_GATE(0x3b);
        SET_GATE(0x3c);
        SET_GATE(0x3d);
        SET_GATE(0x3e);
        SET_GATE(0x3f);
        SET_GATE(0x40);
        SET_GATE(0x41);
        SET_GATE(0x42);
        SET_GATE(0x43);
        SET_GATE(0x44);
        SET_GATE(0x45);
        SET_GATE(0x46);
        SET_GATE(0x47);
        SET_GATE(0x48);
        SET_GATE(0x49);
        SET_GATE(0x4a);
        SET_GATE(0x4b);
        SET_GATE(0x4c);
        SET_GATE(0x4d);
        SET_GATE(0x4e);
        SET_GATE(0x4f);
        SET_GATE(0x50);
        SET_GATE(0x51);
        SET_GATE(0x52);
        SET_GATE(0x53);
        SET_GATE(0x54);
        SET_GATE(0x55);
        SET_GATE(0x56);
        SET_GATE(0x57);
        SET_GATE(0x58);
        SET_GATE(0x59);
        SET_GATE(0x5a);
        SET_GATE(0x5b);
        SET_GATE(0x5c);
        SET_GATE(0x5d);
        SET_GATE(0x5e);
        SET_GATE(0x5f);
        SET_GATE(0x60);
        SET_GATE(0x61);
        SET_GATE(0x62);
        SET_GATE(0x63);
        SET_GATE(0x64);
        SET_GATE(0x65);
        SET_GATE(0x66);
        SET_GATE(0x67);
        SET_GATE(0x68);
        SET_GATE(0x69);
        SET_GATE(0x6a);
        SET_GATE(0x6b);
        SET_GATE(0x6c);
        SET_GATE(0x6d);
        SET_GATE(0x6e);
        SET_GATE(0x6f);
        SET_GATE(0x70);
        SET_GATE(0x71);
        SET_GATE(0x72);
        SET_GATE(0x73);
        SET_GATE(0x74);
        SET_GATE(0x75);
        SET_GATE(0x76);
        SET_GATE(0x77);
        SET_GATE(0x78);
        SET_GATE(0x79);
        SET_GATE(0x7a);
        SET_GATE(0x7b);
        SET_GATE(0x7c);
        SET_GATE(0x7d);
        SET_GATE(0x7e);
        SET_GATE(0x7f);
        SET_GATE(0x80);
        SET_GATE(0x81);
        SET_GATE(0x82);
        SET_GATE(0x83);
        SET_GATE(0x84);
        SET_GATE(0x85);
        SET_GATE(0x86);
        SET_GATE(0x87);
        SET_GATE(0x88);
        SET_GATE(0x89);
        SET_GATE(0x8a);
        SET_GATE(0x8b);
        SET_GATE(0x8c);
        SET_GATE(0x8d);
        SET_GATE(0x8e);
        SET_GATE(0x8f);
        SET_GATE(0x90);
        SET_GATE(0x91);
        SET_GATE(0x92);
        SET_GATE(0x93);
        SET_GATE(0x94);
        SET_GATE(0x95);
        SET_GATE(0x96);
        SET_GATE(0x97);
        SET_GATE(0x98);
        SET_GATE(0x99);
        SET_GATE(0x9a);
        SET_GATE(0x9b);
        SET_GATE(0x9c);
        SET_GATE(0x9d);
        SET_GATE(0x9e);
        SET_GATE(0x9f);
        SET_GATE(0xa0);
        SET_GATE(0xa1);
        SET_GATE(0xa2);
        SET_GATE(0xa3);
        SET_GATE(0xa4);
        SET_GATE(0xa5);
        SET_GATE(0xa6);
        SET_GATE(0xa7);
        SET_GATE(0xa8);
        SET_GATE(0xa9);
        SET_GATE(0xaa);
        SET_GATE(0xab);
        SET_GATE(0xac);
        SET_GATE(0xad);
        SET_GATE(0xae);
        SET_GATE(0xaf);
        SET_GATE(0xb0);
        SET_GATE(0xb1);
        SET_GATE(0xb2);
        SET_GATE(0xb3);
        SET_GATE(0xb4);
        SET_GATE(0xb5);
        SET_GATE(0xb6);
        SET_GATE(0xb7);
        SET_GATE(0xb8);
        SET_GATE(0xb9);
        SET_GATE(0xba);
        SET_GATE(0xbb);
        SET_GATE(0xbc);
        SET_GATE(0xbd);
        SET_GATE(0xbe);
        SET_GATE(0xbf);
        SET_GATE(0xc0);
        SET_GATE(0xc1);
        SET_GATE(0xc2);
        SET_GATE(0xc3);
        SET_GATE(0xc4);
        SET_GATE(0xc5);
        SET_GATE(0xc6);
        SET_GATE(0xc7);
        SET_GATE(0xc8);
        SET_GATE(0xc9);
        SET_GATE(0xca);
        SET_GATE(0xcb);
        SET_GATE(0xcc);
        SET_GATE(0xcd);
        SET_GATE(0xce);
        SET_GATE(0xcf);
        SET_GATE(0xd0);
        SET_GATE(0xd1);
        SET_GATE(0xd2);
        SET_GATE(0xd3);
        SET_GATE(0xd4);
        SET_GATE(0xd5);
        SET_GATE(0xd6);
        SET_GATE(0xd7);
        SET_GATE(0xd8);
        SET_GATE(0xd9);
        SET_GATE(0xda);
        SET_GATE(0xdb);
        SET_GATE(0xdc);
        SET_GATE(0xdd);
        SET_GATE(0xde);
        SET_GATE(0xdf);
        SET_GATE(0xe0);
        SET_GATE(0xe1);
        SET_GATE(0xe2);
        SET_GATE(0xe3);
        SET_GATE(0xe4);
        SET_GATE(0xe5);
        SET_GATE(0xe6);
        SET_GATE(0xe7);
        SET_GATE(0xe8);
        SET_GATE(0xe9);
        SET_GATE(0xea);
        SET_GATE(0xeb);
        SET_GATE(0xec);
        SET_GATE(0xed);
        SET_GATE(0xee);
        SET_GATE(0xef);
        SET_GATE(0xf0);
        SET_GATE(0xf1);
        SET_GATE(0xf2);
        SET_GATE(0xf3);
        SET_GATE(0xf4);
        SET_GATE(0xf5);
        SET_GATE(0xf6);
        SET_GATE(0xf7);
        SET_GATE(0xf8);
        SET_GATE(0xf9);
        SET_GATE(0xfa);
        SET_GATE(0xfb);
        SET_GATE(0xfc);
        SET_GATE(0xfd);
        SET_GATE(0xfe);
        SET_GATE(0xff);

#undef SET_GATE

        // Flush the IDT
        if (flush)
        {
            grace::idt::flush(idt, 256);
        }

        // Enable interrupts
        asm("sti");

        return idt;
    }
}
