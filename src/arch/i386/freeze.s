/*  freeze.s - Symbols for freezing the CPU for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Freezes the computer
.section .text
.global grace_freeze_cpu
.type grace_freeze_cpu, @function
grace_freeze_cpu:

    // Stop interrupts
    cli

    // Halt the CPU
    hlt

    // If the CPU ever wakes up, halt it again
    jmp grace_freeze_cpu

// Set the size of 'grace_freeze_cpu' symbol for debugging purposes
.size grace_freeze_cpu, . - grace_freeze_cpu
