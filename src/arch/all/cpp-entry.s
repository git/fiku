/*  cpp-entry.s - Symbols for freezing the CPU for i386 architecture

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

.section .text
.global cpp_entry__
.type cpp_entry__, @function
cpp_entry__:

    // Setup the stack
    jmp cpp_entry_setup_stack__

// Set the size of 'cpp_entry__' symbol for debugging purposes
.size cpp_entry__, . - cpp_entry__

.section .text
.global cpp_entry_main__
.type cpp_entry_main__, @function
cpp_entry_main__:

    // Call the main function
    call main

    // Execution should never come to this point, and if it ever comes, let
    // 'stop__' to let it handle
    jmp stop__

// Set the size of 'cpp_entry_main__' symbol for debugging purposes
.size cpp_entry_main__, . - cpp_entry_main__
