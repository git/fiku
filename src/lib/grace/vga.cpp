/*  vga.cpp - VGA related C function implementations

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <grace/vga.h>

#include <string.h>
#include <grace/mboard.h>

namespace
{

    // The actual VGA buffer
    uint16_t* vga_buffer_back = reinterpret_cast<uint16_t*>(0xb8000);

    // The VGA buffer where data is stored temporarily before showing
    uint16_t vga_buffer[25 * 80];

    // Cursor position
    vga_curpos cursor_pos;

    // Cursor shape
    vga_cursor_shape cursor_shape;

    // VGA color
    uint16_t color;

    // Scrolls the screen
    void scroll()
    {

        // Move all contents by a line
        memmove(vga_buffer, vga_buffer + 1 * 80, 24 * 80 * sizeof(uint16_t));

        // Fill the last line with spaces
        for (uint16_t i = 0; i < 80; i++)
        {
            vga_buffer[i + 24 * 80] = color | static_cast<uint16_t>(' ');
        }
    }

    // Moves the hardware cursor
    void update_vga_cursor()
    {
        uint16_t location = cursor_pos.y * 80 + cursor_pos.x;

        // Move cursor
        mboard_outb(0x3d4, 0xe);
        mboard_outb(0x3d5, location >> 8);
        mboard_outb(0x3d4, 0xf);
        mboard_outb(0x3d5, location);

        // Set cursor shape
        mboard_outb(0x3d4, 0xa);
        mboard_outb(0x3d5, (mboard_inb(0x3d5) & 0xc0) | cursor_shape.start);
        mboard_outb(0x3d4, 0xb);
        mboard_outb(0x3d5, (mboard_inb(0x3e0) & 0xe0) | cursor_shape.end);
    }
}

void vga_init()
{
    cursor_pos = {0, 0};
    cursor_shape = {0xe, 0xf};
    vga_setcolor(vga_color_light_grey, vga_color_black);
}

void vga_clear()
{
    for (uint16_t i = 0; i < 25 * 80; i++)
    {
        vga_buffer[i] = color | static_cast<uint16_t>(' ');
    }
}

void vga_putc(char ch)
{
    switch (ch)
    {
        case '\0':
        {
            return;
        }

        case '\b':
        {
            cursor_pos.x--;
            break;
        }

        case '\t':
        {
            cursor_pos.x += 8 - cursor_pos.x % 8;
            if (cursor_pos.x >= 80)
            {
                cursor_pos.x = 79;
            }
            break;
        }

        case '\n':
        {
            cursor_pos.x = 0;
            cursor_pos.y++;
            if (cursor_pos.y >= 25)
            {
                cursor_pos.y = 24;
                scroll();
            }
            break;
        }

        case '\r':
        {
            cursor_pos.x = 0;
            break;
        }

        default:
        {
            if (cursor_pos.x >= 80)
            {
                cursor_pos.x = 0;
                cursor_pos.y++;
                if (cursor_pos.y >= 25)
                {
                    cursor_pos.y = 24;
                    scroll();
                }
            }

            uint16_t index = cursor_pos.y * 80 + cursor_pos.x;
            vga_buffer[index] = color | static_cast<uint16_t>(ch);
            cursor_pos.x++;
            break;
        }
    }
}

void vga_flush()
{
    memcpy(vga_buffer_back, vga_buffer, sizeof(vga_buffer));
    update_vga_cursor();
}

void vga_curmove(uint8_t y, uint8_t x)
{
    cursor_pos = {y, x};
}

vga_curpos vga_getcurpos()
{
    return cursor_pos;
}

void vga_write(const char* data, size_t count)
{
    for (size_t i = 0; i < count; i++)
    {
        vga_putc(data[i]);
    }
}

void vga_setcolor(vga_color fg, vga_color bg)
{
    color = (fg | (bg << 4)) << 8;
}

vga_color_pair vga_getcolor()
{
    return
    {
        static_cast<vga_color>((color >> 8) | 0xf),
        static_cast<vga_color>((color >> 12) | 0xf)
    };
}

void vga_setcur(uint8_t start, uint8_t end)
{
    cursor_shape = {start, end};
}

vga_cursor_shape vga_getcur()
{
    return cursor_shape;
}
