/*  string.cpp - Null-terminated byte string related C function implementations

    This file is a part of Grace.
    Copyright (C) 2021 Akib Azmain Turja

    Grace is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Grace is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <string.h>

#include <stdlib.h>

void* memset(void* dest, int ch, size_t count)
{
    for (size_t i = 0; i < count; i++)
    {
        static_cast<unsigned char*>(dest)[i] = static_cast<unsigned char>(ch);
    }

    return dest;
}

void* memcpy(void* dest, const void* src, size_t count)
{

    // Copy bytes
    for (size_t i = 0; i < count; i++)
    {
        reinterpret_cast<unsigned char*>(dest)[i] = reinterpret_cast<const unsigned char*>(src)[i];
    }

    // Return dest
    return dest;
}

void* memmove(void* dest, const void* src, size_t count)
{

    // If the destination and the source is same, do nothing
    if (dest == src)
    {
        return dest;
    }

    // If the destination and the source don't overlap, use memcpy
    else if (static_cast<size_t>(abs(
        reinterpret_cast<unsigned char*>(dest) - reinterpret_cast<const unsigned char*>(src)
    )) >= count)
    {
        return memcpy(dest, src, count);
    }

    // If the destination and the source overlap and source is before destination,
    // then ...
    else if (reinterpret_cast<unsigned char*>(dest) - reinterpret_cast<const unsigned char*>(src) < 0)
    {

        // ... copy bytes from begin to end
        for (size_t i = 0; i < count; i++)
        {
            reinterpret_cast<unsigned char*>(dest)[i] = reinterpret_cast<const unsigned char*>(src)[i];
        }
    }

    // Otherwise ...
    else
    {

        // ... copy bytes from end to begin
        for (size_t i = count; i > 0; i--)
        {
            reinterpret_cast<unsigned char*>(dest)[i - 1] = reinterpret_cast<const unsigned char*>(src)[i - 1];
        }
    }

    // Return dest
    return dest;
}
