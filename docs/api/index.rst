..  Copyright (c)  2021  Akib Azmain Turja.
    Permission is granted to copy, distribute and/or modify this document
    under the terms of the GNU Free Documentation License, Version 1.3
    or any later version published by the Free Software Foundation;
    with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
    A copy of the license is included in the section entitled "GNU
    Free Documentation License".

Grace Internal Library
======================

Grace provides an internal library to make it easy to hack the kernel or extend
it.

.. toctree::
    :maxdepth: 1

    C Standard Library <stdlib/c/index>
    C++ Standard Library <stdlib/cpp/index>
    Grace Internal Platform-specific Library <grace/index>
